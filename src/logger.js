export class Logger {
    static log(message) {
        const paragraph = document.createElement("P");
        const textNode = document.createTextNode(`${(new Date()).toLocaleTimeString('it-IT')}: ${message}`);
        paragraph.appendChild(textNode);
        document.getElementById("logger").appendChild(paragraph);
    }
}