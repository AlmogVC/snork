export function getRandomNumber(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

export function didCollide(firstObjectRow, firstObjectColumn, secondObjectRow, secondObjectColumn) {
    return (firstObjectRow === secondObjectRow && firstObjectColumn === secondObjectColumn);
} 