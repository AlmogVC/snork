import { Laser } from '../models/laser.model.js';
import { config } from '../config.js';
import { didCollide } from '../utils/utils.js';
import { Logger } from '../logger.js';

export class LaserManager {
    constructor() {
        this.lasers = [];
        this.lastTimeFrame = 0;
    }

    add(laser) {
        this.lasers.push(laser);
    }

    checkCollisions(snakes) {
        for (let laserIndex = 0; laserIndex < this.lasers.length; laserIndex++) {
            const laser = this.lasers[laserIndex];

            for (let snakeIndex = 0; snakeIndex < snakes.length; snakeIndex++) {
                const snake = snakes[snakeIndex];

                for (let snakeNodeIndex = 0; snakeNodeIndex < snake.nodes.length; snakeNodeIndex++) {
                    const snakeNode = snake.nodes[snakeNodeIndex];

                    if (didCollide(snakeNode.row, snakeNode.column, laser.row, laser.column)) {
                        Logger.log(`${snake.name} was zapped into two`);
                        this.lasers[laserIndex] = undefined;
                        this.extractDead();

                        return snake.split(snakeNodeIndex);
                    }
                }
            }
        }
    }

    move(timeFrame, snakes) {
        if (timeFrame - this.lastTimeFrame >= config.laser.moveTimer) {
            for (let lasterIndex = 0; lasterIndex < this.lasers.length; lasterIndex++) {
                this.lasers[lasterIndex].move();
            }
            return this.checkCollisions(snakes)
        }
    }

    extractDead() {
        this.lasers = this.lasers.filter(laser => laser);
    }

    draw(canvasManager, mapLocation) {
        this.lasers.forEach(laser => laser.draw(canvasManager, mapLocation.bind(this)));
    }
}