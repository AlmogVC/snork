import { didCollide } from '../utils/utils.js';
import { Pickup } from '../models/pickup.model.js';
import { Logger } from '../logger.js';

export class PickupManager {
    constructor(pickupWidth, pickupHeight) {
        this.pickups = [];
        this.pickupWidth = pickupWidth;
        this.pickupHeight = pickupHeight;
    }

    add(getRandomLocation) {
        const { row, column } = getRandomLocation();

        this.pickups.push(new Pickup(row, column, this.pickupWidth, this.pickupHeight, Pickup.getRandomPickupType()));
    }

    checkCollisions(snake) {
        const snakeHead = snake.getHead();

        for (let pickupIndex = 0; pickupIndex < this.pickups.length; pickupIndex++) {
            const pickup = this.pickups[pickupIndex];

            if (didCollide(snakeHead.row, snakeHead.column, pickup.row, pickup.column)) {
                pickup.action(snake);
                Logger.log(`${snake.name} got ${pickup.name} pickup`);

                this.pickups[pickupIndex] = undefined;
            }
        }
        this.extractDead();
    }

    extractDead() {
        this.pickups = this.pickups.filter(pickup => pickup);
    }

    draw(canvasManager, mapLocation) {
        this.pickups.forEach(pickup => pickup.draw(canvasManager, mapLocation));
    }
}