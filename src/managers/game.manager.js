import { BoardManager } from './board.manager.js';
import { getBoardParameters, getAddedSnakeParameters } from '../userInputs.js';
import { config } from '../config.js';
import { Logger } from '../logger.js';

export class GameManager {
    constructor() {
        this.init();
    }

    init() {
        Logger.log('Game started');
        this.isGameRunning = false;
        const { rowsAmount, columnsAmount, canvasWidth, canvasHeight, isGridDrawn } = getBoardParameters();
        this.boardManager = new BoardManager(
            rowsAmount,
            columnsAmount,
            canvasWidth,
            canvasHeight,
            isGridDrawn,
        );
        this.boardManager.init();
        this.setKeyBoardEventHandlers();
        this.lastTimeFrame = 0;
        this.lastFruitSpawnTimeFrame = 0;
        this.lastPickupSpawnTimeFrame = 0;
        this.lastBombSpawnTimeFrame = 0;

        this.toggleGameState();
        this.toggleGameState();
    }

    addSnake() {
        const { row, column, length, name } = getAddedSnakeParameters();
        this.boardManager.addSnake(row, column, length, name, 1, 0);
    }

    calculateFrame(timeFrame) {
        const frameIdToken = window.requestAnimationFrame((timeFrame) => this.calculateFrame(timeFrame));
        this.renderFrame();
        this.physicsFrame(timeFrame);

        if (this.isGameRunning === false) {
            window.cancelAnimationFrame(frameIdToken);
        }
    }

    toggleGameState() {
        this.isGameRunning = !this.isGameRunning;

        if (this.isGameRunning) {
            this.boardManager.setText('');
            window.requestAnimationFrame((timeFrame) => this.calculateFrame(timeFrame));
        } else {
            this.boardManager.setText('GAME PAUSED');
        }
    }

    physicsFrame(timeFrame) {
        this.boardManager.moveSnakes(timeFrame);
        this.boardManager.moveLasers(timeFrame);

        if (timeFrame - this.lastFruitSpawnTimeFrame >= config.fruit.spawnTimer) {
            this.lastFruitSpawnTimeFrame = timeFrame;
            this.boardManager.addFruit();
        }

        if (timeFrame - this.lastPickupSpawnTimeFrame >= config.pickup.spawnTimer) {
            this.lastPickupSpawnTimeFrame = timeFrame;
            this.boardManager.addPickup();
        }

        if (timeFrame - this.lastBombSpawnTimeFrame >= config.bomb.spawnTimer) {
            this.lastBombSpawnTimeFrame = timeFrame;
            this.boardManager.addBomb();
        }
    }

    renderFrame() {
        this.boardManager.draw();
    }

    setKeyBoardEventHandlers() {
        window.onkeydown = (event) => {
            const keyPressed = event.keyCode ? event.keyCode : event.which;
            switch (keyPressed) {
                case config.inputButtons.down:
                    this.boardManager.setSnakesDirection(1, 0);
                    break;
                case config.inputButtons.up:
                    this.boardManager.setSnakesDirection(-1, 0);
                    break;
                case config.inputButtons.left:
                    this.boardManager.setSnakesDirection(0, -1);
                    break;
                case config.inputButtons.right:
                    this.boardManager.setSnakesDirection(0, 1);
                    break;
                case config.inputButtons.pause:
                    this.toggleGameState();
                    break;
                case config.inputButtons.restart:
                    this.init();
                    break;
                case config.inputButtons.shootLaser:
                    this.boardManager.shootLaser();
                    break;
                default:
                    break;
            }
        }
    }
}