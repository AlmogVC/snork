import { Node } from '../models/node.model.js';
import { Color } from '../models/color.model.js';
import { getRandomNumber, didCollide } from '../utils/utils.js';

export class FruitManager {
    constructor(fruitsRadius) {
        this.fruits = [];
        this.fruitsRadius = fruitsRadius;
    }

    addFruit(getRandomLocation) {
        const { row, column } = getRandomLocation();
        const color = new Color(0, getRandomNumber(0, 255), 0, 1);

        this.fruits.push(new Node(row, column, this.fruitsRadius, color, color));
    }

    checkCollisions(snake) {
        const snakeHead = snake.getHead();

        for (let fruitIndex = 0; fruitIndex < this.fruits.length; fruitIndex++) {
            const fruit = this.fruits[fruitIndex];

            if (didCollide(snakeHead.row, snakeHead.column, fruit.row, fruit.column)) {
                snake.eat(fruit);
                this.fruits[fruitIndex] = undefined;
            }
        }
        
        this.extractDeadFruits();
    }

    extractDeadFruits() {
        this.fruits = this.fruits.filter(fruit => fruit);
    }

    draw(canvasManager, mapLocation) {
        this.fruits.forEach(fruit => fruit.draw(canvasManager, mapLocation));
    }
}