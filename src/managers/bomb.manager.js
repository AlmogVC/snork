import { Bomb } from '../models/bomb.model.js';
import { didCollide } from '../utils/utils.js';
import { Logger } from '../logger.js';

export class BombManager {
    constructor(bombRadius) {
        this.bombs = [];
        this.bombRadius = bombRadius;
    }

    add(getRandomLocation) {
        const { row, column } = getRandomLocation();

        this.bombs.push(new Bomb(row, column, this.bombRadius));
    }

    checkCollisions(snake) {
        const snakeHead = snake.getHead();

        for (let bombIndex = 0; bombIndex < this.bombs.length; bombIndex++) {
            const bomb = this.bombs[bombIndex];

            if (didCollide(snakeHead.row, snakeHead.column, bomb.row, bomb.column)) {
                Logger.log(`${snake.name} exploded`);
                snake.die();
                this.bombs[bombIndex] = undefined;
            }
        }

        this.extractDead();
    }

    extractDead() {
        this.bombs = this.bombs.filter(bomb => bomb);
    }

    draw(canvasManager, mapLocation) {
        this.bombs.forEach(bomb => bomb.draw(canvasManager, mapLocation.bind(this)));
    }
}