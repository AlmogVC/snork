import { didCollide } from '../utils/utils.js';
import { Logger } from '../logger.js';
import { Snake } from '../models/snake.model.js';

export class SnakeManager {
    constructor(nodeRadius) {
        this.snakes = [];
        this.nodeRadius = nodeRadius;
    }

    moveSnakes(timeFrame, didHitWall, checkFruitCollisions, checkBombCollisions, checkPickupCollisions) {
        for (let snakeIndex = 0; snakeIndex < this.snakes.length; snakeIndex++) {
            if (this.snakes[snakeIndex].isAlive && this.snakes[snakeIndex].shouldMove(timeFrame)) {
                const snakeHead = this.snakes[snakeIndex].move(timeFrame);

                if (didHitWall(snakeHead.row, snakeHead.column)) {
                    Logger.log(`${this.snakes[snakeIndex].name} hit a wall and died`);
                    this.snakes[snakeIndex].die();
                }
                this.snakes[snakeIndex].checkCollisions(checkFruitCollisions, checkBombCollisions, checkPickupCollisions);
                this.checkSnakeOnSnakeCollisions(snakeIndex);
            }
        }
        this.extractDead();
    }

    checkSnakeOnSnakeCollisions(snakeIndex) {
        const snakeHead = this.snakes[snakeIndex].getHead();

        for (let otherSnakeIndex = 0; otherSnakeIndex < this.snakes.length; otherSnakeIndex++) {
            for (let otherSnakeNodeIndex = 0; otherSnakeNodeIndex < this.snakes[otherSnakeIndex].nodes.length; otherSnakeNodeIndex++) {
                const otherSnakeNode = this.snakes[otherSnakeIndex].nodes[otherSnakeNodeIndex];

                if (!(otherSnakeIndex === snakeIndex && otherSnakeNodeIndex === 0)) {
                    if (didCollide(snakeHead.row, snakeHead.column, otherSnakeNode.row, otherSnakeNode.column)) {
                        if (snakeIndex === otherSnakeIndex) {
                            Logger.log(`${this.snakes[snakeIndex].name} touched himself and died`);
                        } else {
                            Logger.log(`${this.snakes[snakeIndex].name} touched ${this.snakes[otherSnakeIndex].name} and died`);
                        }
                        this.snakes[snakeIndex].die();
                    }
                }
            }
        }
    }

    setSnakesDirection(row, column) {
        this.snakes.forEach(snake => snake.setMovementDirection(row, column));
    }

    add(row, column, length, name, rowMovement, columnMovement) {
        Logger.log(`${name} joined the game`);

        this.snakes.push(new Snake(row, column, this.nodeRadius, length, name, rowMovement, columnMovement));
    }

    addExisting(snake) {
        this.snakes.push(snake)
    }

    extractDead() {
        this.snakes = this.snakes.filter(snake => snake.isAlive);
    }

    draw(canvasManager, mapLocation) {
        this.snakes.forEach(snake => snake.draw(canvasManager, mapLocation.bind(this)));
    }
}