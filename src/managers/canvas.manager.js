export class CanvasManager {
    constructor(width, height) {
        this.canvas = document.getElementById('canvas');
        this.context = this.canvas.getContext('2d');
        this.canvas.width = width;
        this.canvas.height = height;
    }

    drawCircle(centerX, centerY, radius, fillColor, strokeColor) {
        this.context.fillStyle = fillColor;
        this.context.strokeStyle = strokeColor;
        this.context.beginPath();
        this.context.arc(centerX, centerY, radius, 0, 2 * Math.PI);
        this.context.stroke();
        this.context.fill();
        this.context.closePath();
    }

    drawHalfCircle(centerX, centerY, radius, fillColor, strokeColor) {
        this.context.fillStyle = fillColor;
        this.context.strokeStyle = strokeColor;
        this.context.beginPath();
        this.context.arc(centerX, centerY, radius, Math.PI, 2 * Math.PI);
        this.context.stroke();
        this.context.fill();
        this.context.closePath();
    }

    drawLine(startX, startY, endX, endY, color) {
        this.context.strokeStyle = color;
        this.context.beginPath();
        this.context.moveTo(startX, startY);
        this.context.lineTo(endX, endY);
        this.context.closePath();
        this.context.stroke();
    }

    drawRectangle(startX, startY, width, height, fillColor, strokeColor) {
        this.context.fillStyle = fillColor;
        this.context.strokeStyle = strokeColor;
        this.context.strokeRect(startX, startY, width, height);
        this.context.fillRect(startX, startY, width, height);
    }

    drawTriangle(position1X, position1Y, position2X, position2Y, position3X, position3Y, fillColor, strokeColor) {
        this.context.fillStyle = fillColor;
        this.context.strokeStyle = strokeColor;
        this.context.beginPath();
        this.context.moveTo(position1X, position1Y);
        this.context.lineTo(position2X, position2Y);
        this.context.lineTo(position3X, position3Y);
        this.context.closePath();
        this.context.fill();
        this.context.stroke();
    }

    drawText(positionX, positionY, size, text) {
        this.context.font = `${size}px Georgia`;
        this.context.fillStyle = 'black';
        this.context.textAlign = 'center';
        this.context.fillText(text, positionX, positionY);
    }

    clear() {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }
}
