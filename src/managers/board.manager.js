import { CanvasManager } from './canvas.manager.js';
import { getRandomNumber } from '../utils/utils.js';
import { FruitManager } from './fruit.manager.js';
import { BombManager } from './bomb.manager.js';
import { PickupManager } from './pickup.manager.js';
import { SnakeManager } from './snake.manager.js';
import { LaserManager } from './laser.manager.js';

export class BoardManager {
    constructor(rowsAmount, columnsAmount, canvasWidth, canvasHeight, isGridDrawn) {
        this.rowsAmount = rowsAmount;
        this.columnsAmount = columnsAmount;
        this.canvasWidth = canvasWidth;
        this.canvasHeight = canvasHeight;
        this.isGridDrawn = isGridDrawn;
        this.currentText = '';
        this.columnWidth = this.canvasWidth / this.columnsAmount;
        this.rowHeight = this.canvasHeight / this.rowsAmount;
        this.fruitManager = new FruitManager(Math.min(this.columnWidth, this.rowHeight) / 3);
        this.bombManager = new BombManager(Math.min(this.columnWidth, this.rowHeight) / 3);
        this.pickupManager = new PickupManager(this.columnWidth, this.rowHeight);
        this.snakeManager = new SnakeManager(Math.min(this.columnWidth, this.rowHeight) / 2);
        this.laserManager = new LaserManager();
        this.canvasManager = new CanvasManager(canvasWidth, canvasHeight);
    }

    setText(text) {
        this.currentText = text;
    }

    init() {
        this.addSnake(this.rowsAmount / 2, this.columnsAmount / 2, 20, 'Almog', 1, 0);
    }

    moveSnakes(timeFrame) {
        this.snakeManager.moveSnakes(
            timeFrame,
            this.didHitWall.bind(this),
            this.fruitManager.checkCollisions.bind(this.fruitManager),
            this.bombManager.checkCollisions.bind(this.bombManager),
            this.pickupManager.checkCollisions.bind(this.pickupManager),
        );
    }

    moveLasers(timeFrame) {
        const snake = this.laserManager.move(timeFrame, this.snakeManager.snakes);

        if (snake) {
            this.snakeManager.addExisting(snake);
        }
    }

    shootLaser() {
        this.snakeManager.snakes.forEach(snake => {
            const laser = snake.shootLaser();

            if (laser) {
                this.laserManager.add(laser);
            }
        });
    }

    setSnakesDirection(row, column) {
        this.snakeManager.setSnakesDirection(row, column);
    }

    didHitWall(row, column) {
        return (
            row < 0 ||
            row >= this.rowsAmount ||
            column < 0 ||
            column >= this.columnsAmount);
    }

    addFruit() {
        this.fruitManager.addFruit(this.getRandomLocation.bind(this));
    }

    addBomb() {
        this.bombManager.add(this.getRandomLocation.bind(this));
    }

    addPickup() {
        this.pickupManager.add(this.getRandomLocation.bind(this));
    }

    addSnake(row, column, length, name, rowMovement, columnMovement) {
        this.snakeManager.add(row, column, length, name, rowMovement, columnMovement);
    }

    draw() {
        this.canvasManager.clear();
        if (this.isGridDrawn) this.drawGrid();
        this.fruitManager.draw(this.canvasManager, this.mapLocation.bind(this));
        this.pickupManager.draw(this.canvasManager, this.mapLocation.bind(this));
        this.bombManager.draw(this.canvasManager, this.mapLocation.bind(this));
        this.snakeManager.draw(this.canvasManager, this.mapLocation.bind(this));
        this.laserManager.draw(this.canvasManager, this.mapLocation.bind(this));
        this.drawText();
    }

    drawText() {
        const size = (Math.min(this.canvasWidth, this.canvasHeight) * 2) / Math.min(this.rowsAmount, this.columnsAmount);
        this.canvasManager.drawText(this.canvasWidth / 2, this.canvasHeight / 2, size, this.currentText);
    }

    drawGrid() {
        for (let columnIndex = 0; columnIndex <= this.columnsAmount; columnIndex++) {
            const lineStartX = columnIndex * this.columnWidth;
            const lineStartY = 0;
            const lineEndX = columnIndex * this.columnWidth;
            const lineEndY = this.canvasHeight;
            this.canvasManager.drawLine(lineStartX, lineStartY, lineEndX, lineEndY, "blue");
        }

        for (let rowIndex = 0; rowIndex <= this.rowsAmount; rowIndex++) {
            const lineStartX = 0;
            const lineStartY = rowIndex * this.rowHeight;
            const lineEndX = this.canvasWidth;
            const lineEndY = rowIndex * this.rowHeight;
            this.canvasManager.drawLine(lineStartX, lineStartY, lineEndX, lineEndY, "blue");
        }
    }

    getRandomLocation() {
        return {
            row: getRandomNumber(0, this.rowsAmount - 1),
            column: getRandomNumber(0, this.columnsAmount - 1),
        };
    }

    mapLocation(row, column, isCentered) {
        const location = {
            x: (column / this.columnsAmount) * this.canvasWidth,
            y: (row / this.rowsAmount) * this.canvasHeight,
        }

        if (isCentered) {
            location.x += this.columnWidth / 2;
            location.y += this.rowHeight / 2;
        }

        return location;
    }
}