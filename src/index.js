import { GameManager } from './managers/game.manager.js';

const gameManager = new GameManager();

window.restart = () => gameManager.init();
window.pause = () => gameManager.toggleGameState();
window.addSnake = () => gameManager.addSnake();