export const config = {
    canvas: {
        width: 2000,
        height: 2000,
    },
    board: {
        rowsAmount: 30,
        columnsAmount: 30,
    },
    snake: {
        nodeResizeWhenContainsFood: 1.4,
        name: 'Snake',
        length: 1,
        row: 1,
        column: 1,
        startSpeed: 700,
        moveTimer: 1000,
    },
    laser: {
        moveTimer: 100,
    },
    fruit: {
        spawnTimer: 6000,
    },
    bomb: {
        spawnTimer: 10000,
    },
    pickup: {
        spawnTimer: 8000,
    },
    inputButtons: {
        up: 87,
        down: 83,
        left: 65,
        right: 68,
        pause: 80,
        restart: 13,
        shootLaser: 17,
    }
}