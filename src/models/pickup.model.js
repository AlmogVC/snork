import { getRandomNumber } from '../utils/utils.js';

export class Pickup {
    constructor(row, column, width, height, type) {
        this.row = row;
        this.column = column;
        this.width = width;
        this.height = height;
        this.color = type.color;
        this.action = type.action;
        this.name = type.name
    }

    draw(canvasManager, locationMapper) {
        const { x, y } = locationMapper(this.row, this.column, false);

        canvasManager.drawRectangle(x, y, this.width, this.height, this.color, this.color);
    }

    static getRandomPickupType() {
        const pickupTypesArray = Object.values(PickUpTypes);
        const randomIndex = getRandomNumber(0, pickupTypesArray.length - 1);

        return pickupTypesArray[randomIndex];
    }
}

export const PickUpTypes = {
    speedUp: {
        name: "speed up",
        color: 'yellow',
        action: (snake) => snake.accelerate(getRandomNumber(10, 20)),
    },
    speedDown: {
        name: "speed down",
        color: 'blue',
        action: (snake) => snake.accelerate((-1) * getRandomNumber(10, 20)),
    },
    laser: {
        name: "laser",
        color: 'red',
        action: (snake) => { snake.addLaser() },
    },
}
