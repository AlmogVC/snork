import { Node } from "./node.model.js";
import { Color } from "./color.model.js";
import { Laser } from "./laser.model.js";
import { config } from "../config.js";

export class Snake {
    constructor(row, column, nodeRadius, length, name, rowMovement, columnMovement, nodes) {
        this.row = row;
        this.column = column;
        this.length = nodes ? nodes.length : length;
        this.nodeRadius = nodeRadius;
        this.name = name;
        this.nodes = nodes || [];
        this.setMovementDirection(rowMovement, columnMovement)
        if (!nodes) this.init();
        this.isAlive = true;
        this.speed = config.snake.startSpeed;
        this.lastTimeFrame = 0;
        this.lasersAmount = 0;
    }

    init() {
        let row = this.row;
        let column = this.column;

        for (let nodeIndex = 0; nodeIndex < this.length; nodeIndex++) {
            this.addNode(row, column);
            row -= this.rowMovement;
            column -= this.columnMovement;
        }
    }

    addLaser() {
        this.lasersAmount++;
    }

    shootLaser() {
        if (this.lasersAmount) {
            this.lasersAmount--;
            const direction = { rowMovement: this.rowMovement, columnMovement: this.columnMovement };
            const head = this.getHead();
            return new Laser(head.row + direction.rowMovement, head.column + direction.columnMovement, this.nodeRadius / 2, direction);
        }
    }

    split(nodeIndex) {
        const newSnakeNodes = this.nodes.slice(0, nodeIndex);
        let newSnake;

        if (newSnakeNodes && newSnakeNodes.length >= 1) {
            newSnake = new Snake(
                newSnakeNodes[0].row,
                newSnakeNodes[0].column,
                this.nodeRadius,
                newSnakeNodes.length,
                `Splitted ${this.name}`,
                this.rowMovement,
                this.columnMovement,
                newSnakeNodes);
        }

        this.nodes = this.nodes.slice(nodeIndex + 1, this.nodes.length);

        if (this.nodes.length === 0) {
            this.die();
        }

        return newSnake;
    }

    accelerate(acceleration) {
        this.speed += acceleration;
    }

    eat(fruitNode) {
        this.nodes[0].containedFood = fruitNode;
    }

    die() {
        this.isAlive = false;
    }

    setMovementDirection(rowDirection, columnDirection) {
        if (this.rowMovement != rowDirection * (-1) && this.columnMovement != columnDirection * (-1)) {
            this.rowMovement = rowDirection;
            this.columnMovement = columnDirection;
        }
    }

    getHead() {
        return this.nodes[0];
    }

    expand(node) {
        node.radius = this.nodeRadius;
        this.nodes.push(node);
    }

    shouldMove(timeFrame) {
        if (timeFrame - this.lastTimeFrame >= config.snake.moveTimer - this.speed) {
            this.lastTimeFrame = timeFrame;
            return true;
        }

        return false;
    }

    move() {
        for (let nodeIndex = this.nodes.length - 1; nodeIndex >= 0; nodeIndex--) {
            if (this.nodes[nodeIndex].containedFood) {
                if (nodeIndex === this.nodes.length - 1) {
                    this.expand(this.nodes[nodeIndex].containedFood);
                    this.nodes[nodeIndex].containedFood = undefined;
                } else {
                    this.nodes[nodeIndex + 1].containedFood = this.nodes[nodeIndex].containedFood;
                    this.nodes[nodeIndex].containedFood = undefined;
                }
            }

            if (nodeIndex >= 1) {
                this.nodes[nodeIndex].row = this.nodes[nodeIndex - 1].row
                this.nodes[nodeIndex].column = this.nodes[nodeIndex - 1].column
            } else {
                this.nodes[0].row += this.rowMovement;
                this.nodes[0].column += this.columnMovement;
            }
        }

        return this.nodes[0]
    }


    checkCollisions(checkFruitCollisions, checkBombCollisions, checkPickupCollisions) {
        checkFruitCollisions(this);
        checkBombCollisions(this);
        checkPickupCollisions(this);
    }

    addNode(row, column) {
        const nodeColor = this.getNextNodeColor();
        this.nodes.push(new Node(row, column, this.nodeRadius, nodeColor, nodeColor));
    }

    getNextNodeColor() {
        let color;

        if (this.nodes.length === 0) {
            color = new Color(0, 255, 0, 1);
        } else {
            color = new Color(0, this.nodes[this.nodes.length - 1].color.green -= 5 || 0, 0, 1);
        }

        return color;
    }

    draw(canvasManager, locationMapper) {
        for (let nodeIndex = this.nodes.length - 1; nodeIndex >= 0; nodeIndex--) {
            this.nodes[nodeIndex].draw(canvasManager, locationMapper);
        }
    }
}