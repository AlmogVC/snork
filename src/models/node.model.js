import { config } from "../config.js";

export class Node {
    constructor(row, column, radius, color, borderColor) {
        this.row = row;
        this.column = column;
        this.radius = radius;
        this.color = color;
        this.borderColor = borderColor;
        this.containedFood;
    }

    draw(canvasManager, locationMapper) {
        const { x, y } = locationMapper(this.row, this.column, true);
        let radius = this.radius;

        if (this.containedFood) {
            radius *= config.snake.nodeResizeWhenContainsFood;
        }

        canvasManager.drawCircle(x, y, radius, this.color.getColorString(), this.borderColor.getColorString());
    }
}