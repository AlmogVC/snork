export class Laser {
    constructor(row, column, radius, direction) {
        this.row = row;
        this.column = column;
        this.radius = radius;
        this.direction = direction;
    }

    draw(canvasManager, locationMapper) {
        const { x, y } = locationMapper(this.row, this.column, true);

        canvasManager.drawCircle(x, y, this.radius, 'red', 'red');
    }

    move() {
        this.row += this.direction.rowMovement;
        this.column += this.direction.columnMovement;
    }
}