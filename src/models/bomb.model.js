export class Bomb {
    constructor(row, column, radius) {
        this.row = row;
        this.column = column;
        this.radius = radius;
    }

    draw(canvasManager, locationMapper) {
        const { x, y } = locationMapper(this.row, this.column, true);

        canvasManager.drawHalfCircle(x, y, this.radius, 'black', 'black');
    }
}