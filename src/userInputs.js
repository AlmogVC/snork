import { config } from './config.js';

export function getBoardParameters() {
    return {
        canvasWidth: Number(document.getElementById('canvasWidth').value || config.canvas.width),
        canvasHeight: Number(document.getElementById('canvasHeight').value || config.canvas.height),
        rowsAmount: Number(document.getElementById('rowsAmount').value || config.board.rowsAmount),
        columnsAmount: Number(document.getElementById('columnsAmount').value || config.board.columnsAmount),
        isGridDrawn: Boolean(document.getElementById('showGrid').checked),
    }
}

export function getAddedSnakeParameters() {
    return {
        length: Number(document.getElementById('snakeLength').value || config.snake.length),
        name: document.getElementById('snakeName').value || config.snake.name,
        row: Number(document.getElementById('snakeRow').value || config.snake.row),
        column: Number(document.getElementById('snakeColumn').value || config.snake.column),
    }
}
